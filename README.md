## **Identification of circular RNAs sequence variations between conditions using k-mer sequences: SPLASH tool extension for circRNAs**
### **Overview**
This script extends the functionality of the SPLASH tool (Chaung _et al._ Cell 2023, <https://doi.org/10.1016/j.cell.2023.10.028>) for circular RNAs (circRNAs). It specifically targets the identification of anchor:target pairs identified by SPLASH within circRNAs. Due to the loop structure of circRNAs, it employs a reverse search approach and annotates them using the circBase database (Glazar _et al._ RNA 2014, <https://doi.org/10.1261/rna.043687.113>). This script will then allows to identify sequence variations within circular RNAs and could confirm their biomarker potential.

### **Usage**
To use this script, you can download the input files available in the ```example/``` folder: 

1. Install ```python3```
2. Follow the provided syntax:
```python3 search_kmer_circrna_sequence.py anchor_target_sequences.txt circRNA_sequences.fa output_file.txt```

* anchor_target_sequence.txt: text file containing the anchor:target pairs identified by SPLASH
* circRNA_sequences.txt: FASTA file containing the circRNA sequences given by circBase
* output_file.txt: name of your output file

