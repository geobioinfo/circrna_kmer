import sys

def find_kmer_pairs(sequence, anchor, target):
    kmer_length = len(anchor)
    pairs = []  # Store positions of k-mers

    anchor_positions = [i for i in range(len(sequence) - kmer_length + 1) if sequence[i:i + kmer_length] == anchor]
    target_positions = [i for i in range(len(sequence) - kmer_length + 1) if sequence[i:i + kmer_length] == target]

    for anchor_pos in anchor_positions:
        for target_pos in target_positions:
            if target_pos < anchor_pos and target_pos + kmer_length == anchor_pos: # search pairs without any gap 
                pairs.append((target_pos, anchor_pos))

    return pairs

def extract_coordinates(header):
    parts = header.split("|")
    circrna_id = parts[0].strip()
    circrna_info = parts[1].split(":")
    coordinates = circrna_info[1].strip().split('-')
    chromosome = circrna_info[0].strip()
    return circrna_id, chromosome, coordinates

def process_input_files(kmer_file, circrna_file, output_file):
    with open(kmer_file, 'r') as f:
        next(f)  # skip first iteration = first line corresponding to the header
        kmer_pairs = [line.strip().split() for line in f]
        total_kmers = len(kmer_pairs) # to print the total number of kmer in the terminal

    results = []

    with open(circrna_file, 'r') as f:
        header = ""
        sequence = ""
        for line in f:
            if line.startswith('>'):
                if header:
                    circrna_id, chromosome, coordinates = extract_coordinates(header)
                    for idx, (kmer1, kmer2) in enumerate(kmer_pairs, start=1):
                        pairs = find_kmer_pairs(sequence, kmer1, kmer2)
                        if pairs:
                            results.append((kmer1, kmer2, circrna_id, chromosome, coordinates, pairs))
                        print(f"Processed {idx}/{total_kmers} kmers for circRNA {circrna_id}")
                header = line[1:].strip()
                sequence = ""
            else:
                sequence += line.strip()

        # Process the last circRNA
        circrna_id, chromosome, coordinates = extract_coordinates(header)
        for idx, (kmer1, kmer2) in enumerate(kmer_pairs, start=1):
            pairs = find_kmer_pairs(sequence, kmer1, kmer2)
            if pairs:
                results.append((kmer1, kmer2, circrna_id, chromosome, coordinates, pairs))
            print(f"Processed {idx}/{total_kmers} kmers for circRNA {circrna_id}")

    with open(output_file, 'w') as f:
        f.write("target\tanchor\tcircrna_id\tchromosome\tposition\n")
        for kmer1, kmer2, circrna_id, chromosome, coordinates, pairs in results:
            formatted_pairs = [(start + int(coordinates[0]), end + int(coordinates[0])) for start, end in pairs]
            f.write(f"{kmer2}\t{kmer1}\t{circrna_id}\t{chromosome}\t{formatted_pairs}\n")

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python3 script.py kmer_file circrna_file output_file")
        sys.exit(1)

    kmer_file = sys.argv[1]
    circrna_file = sys.argv[2]
    output_file = sys.argv[3]

    process_input_files(kmer_file, circrna_file, output_file)
